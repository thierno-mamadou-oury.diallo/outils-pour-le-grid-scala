import scala.io.Source

object Main {
  def main(args: Array[String]) {

    def getConnection(v:String)=List(v.split(" "))

    val filename = "/absolute_path/file.txt"   //Give path for fileConnection
    var connection :List[String] = Source.fromFile(filename).getLines.toList ;
    val connectionList = connection.flatten(e=>getConnection(e))
    val conn = new Connection(connectionList)

    val connectedserver = conn.getConnectionList(args.last)
    println("Serveurs connectées :")
    connectedserver.foreach(
        e =>
          if(!e.equals(()))
            println(e)
    )

    println("Connexion récente :" + conn.getLastConnection())
  }
}