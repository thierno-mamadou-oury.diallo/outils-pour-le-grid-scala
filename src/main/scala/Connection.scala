

trait Trait {
  def getConnectionList(ip: String): List[Any]
  def getTimeStamp(): Long
  def getLastConnection(): String
}

class Connection(list: List[Array[String]]) extends Trait{
  var l: List[Array[String]]= list

  def getConnectionList(host2: String): List[Any] = {

    val listConnected = List.empty[String]
    def flat(v:Array[String]) : Any =
      if (v(2) == host2)
        v(1)

    l.map(e=> flat(e))
  }

  def getLastConnection(): String = {

    var lastConnection : String = ""

    l.foreach(e => lastConnection = e.last)

    lastConnection
  }

  def getTimeStamp(): Long = {

    1623

  }

}